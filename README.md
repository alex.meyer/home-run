# home-run

Case study about a defence hunting the quarterback

# Run and test the prototype

Just open the project in Unity (2021.3.4f1 LTS), open the scene HomeRun and press play.
The project uses the StarterAssets provided by Unity. This includes a third person controller slightly modified.
To control the character you can use the arrow keys. To switch from walk to run press the Shift key.
The game starts immediately and the defence is trying to tackle the player.
The player has to bypass the defence players and reach the home run zone.
If the player gets tackled the game is over.
The player can restart the game after being tackled or having a home run by pressing the "1" key.

# Technincal aspects

## EventSO / DataSO

The game makes heavy use of ScriptableObjects events and variables. Values which needs to be populated to other components are fed into variables of specific kind derived from BaseVariable<T>. Components can poll the values from these "mini models" or subscribe for changes by event.
Instead of using the C# delegate system this project relys on anonymous subscription by EventSO. It is working in the same way but the signature contains the EventSO as parameter. In this way it is easy to transport any kind of data between several components. If a return value is needed the EventSO can contain a callback reference to be called in the end or even several for specific states.

## Character configuration

The player and also the opponent prefab is fed by CharacterConfig, a ScriptableObject based data asset. For now it just contains walk and run speed.

## micro management

To stay mostly indpendent the project makes heavy use of the compoents appended to game objects. Eg the original ThirdPersonController of the starter asset is not touched at all. Instead it is being fed by components added to the game object containing the CharacterController and the ThirdPersonController.
Nevertheless the prefab of the player is being altered to switch off some not needed features in the ccamera system and the complete UI.

## Follow script

The game takes over in the moment the player catched the football and starts to along the field to prevent contact with the defence players. The defence prefab contains a component to follow the player in terms to catch him in time. Therefor the position of the player in the next frame is being used by calculating it by the current speed and the direction the player is currently running.
The time amount for the prediction can be raised by a time value. For now it is using just one frame (0.033f seconds).

## Tackle situation

If an opponent gets near enough to the player it is causing a tackle animation and throwing an event that player and other opponents are informed the tackling happens.
To have a better view there are 3 animations from mixamo added to the project and the AnimatorController of the original asset.

## Home Run situation

If the player managed to reach the home run zone he celebrates it and the opponents are stopping to hunt him. They are informed by an event. The situation of home run zone is realized by a trigger collider over the whole area.

# Time

I worked on this implemetation approcimately 8 hours.


