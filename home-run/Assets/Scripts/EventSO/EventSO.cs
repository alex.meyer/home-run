using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace HomeRun
{
    [CreateAssetMenu(fileName = "NewGameEvent", menuName = "EventSO/GameEvent")]
    public class EventSO : ScriptableObject
    {
        [TextArea(3, 5)] public string description;

        private Action<EventSO> _onEvent;

        public void Invoke()
        {
            InternalRaise();
        }

        private void InternalRaise()
        {
            _onEvent?.Invoke(this);
        }

        public void RegisterListener(Action<EventSO> listener)
        {
            _onEvent += listener;
        }

        public void UnregisterListener(Action<EventSO> listener)
        {
            _onEvent -= listener;
        }
    }
}
