using System;
using StarterAssets;
using UnityEngine;

namespace HomeRun
{
    [RequireComponent(typeof(ThirdPersonController))]
    public class ConfigCharacter : MonoBehaviour
    {
        [SerializeField] private CharacterConfig characterConfig;
        [SerializeField] private ThirdPersonController thirdPersonController;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void Start()
        {
            Init();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void Init()
        {
            thirdPersonController.MoveSpeed = characterConfig.MoveSpeed;
            thirdPersonController.SprintSpeed = characterConfig.RunSpeed;
        }

        private void RegisterListeners() { }

        private void UnregisterListeners() { }

        private void ValidateReferences()
        {
            thirdPersonController = GetComponent<ThirdPersonController>();

            Debug.Assert(thirdPersonController != null, "thirdPersonController != null");
            Debug.Assert(characterConfig != null, "characterConfig != null");
        }

    }
}