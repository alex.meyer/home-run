using System;
using UnityEngine;

namespace HomeRun
{
    [RequireComponent(typeof(CharacterController))]
    public class PopulateCharacterValues : MonoBehaviour
    {
        [SerializeField] private Vector3Variable characterSpeedVariable;
        [SerializeField] private Vector3Variable characterPositionVariable;
        [SerializeField] private CharacterController characterController;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void Update()
        {
            characterSpeedVariable.Content = characterController.velocity;
            characterPositionVariable.Content = characterController.transform.position;
        }

        private void RegisterListeners() { }

        private void UnregisterListeners() { }

        private void ValidateReferences()
        {
            characterController = GetComponent<CharacterController>();

            Debug.Assert(characterSpeedVariable != null, "characterSpeedVariable != null");
            Debug.Assert(characterPositionVariable != null, "characterPositionVariable != null");
            Debug.Assert(characterController != null, "characterController != null");
        }

    }
}