using StarterAssets;
using UnityEngine;

namespace HomeRun
{
    [RequireComponent(typeof(ThirdPersonController), typeof(Animator))]
    public class PlayerTackledReceiver : MonoBehaviour
    {
        private static readonly int hashBeingTackled = Animator.StringToHash("BeingTackled");

        [Header("Events")]
        [SerializeField] private EventSO playerTackledEventSo;

        [Header("Components")]
        [SerializeField] private ThirdPersonController thirdPersonController;
        [SerializeField] private Animator animator;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void Tackled(EventSO ev)
        {
            thirdPersonController.enabled = false;
            animator.SetTrigger(hashBeingTackled);
        }

        private void RegisterListeners()
        {
            playerTackledEventSo.RegisterListener(Tackled);
        }

        private void UnregisterListeners()
        {
            playerTackledEventSo.UnregisterListener(Tackled);
        }

        private void ValidateReferences()
        {
            thirdPersonController = GetComponent<ThirdPersonController>();
            animator = GetComponent<Animator>();

            Debug.Assert(playerTackledEventSo != null, "playerTackledEvent != null");
            Debug.Assert(thirdPersonController != null, "thirdPersonController != null");
            Debug.Assert(animator != null, "animator != null");
        }

    }
}