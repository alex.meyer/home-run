using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace HomeRun
{
    public class RestartIfGameStopped : MonoBehaviour
    {
        [SerializeField] private EventSO homeRunEvent;
        [SerializeField] private EventSO playerTackledEvent;

        private bool _gameStopped = false;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
            _gameStopped = false;
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void Update()
        {
            if (!_gameStopped)
            {
                return;
            }

            if (Keyboard.current.digit1Key.wasPressedThisFrame)
            {
                SceneManager.LoadScene("HomeRun");
            }
        }

        private void GameStopped(EventSO ev)
        {
            _gameStopped = true;
        }

        private void RegisterListeners()
        {
            homeRunEvent.RegisterListener(GameStopped);
            playerTackledEvent.RegisterListener(GameStopped);
        }

        private void UnregisterListeners()
        {
            homeRunEvent.UnregisterListener(GameStopped);
            playerTackledEvent.UnregisterListener(GameStopped);
        }

        private void ValidateReferences()
        {
            Debug.Assert(homeRunEvent != null, "homeRunEvent != null");
            Debug.Assert(playerTackledEvent != null, "playerTackledEvent != null");
        }

    }
}