using System;
using UnityEngine;

namespace HomeRun
{
    public class HomeRunDetector : MonoBehaviour
    {
        [SerializeField] private EventSO homeRunEvent;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void RegisterListeners() { }

        private void UnregisterListeners() { }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player"))
            {
                return;
            }

            homeRunEvent.Invoke();
        }

        private void ValidateReferences()
        {
            Debug.Assert(homeRunEvent != null, "homeRunEvent != null");
        }

    }
}