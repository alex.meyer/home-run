using StarterAssets;
using UnityEngine;
using UnityEngine.UIElements;

namespace HomeRun
{
    [RequireComponent(typeof(Animator), typeof(ThirdPersonController))]
    public class HomeRunReceiver : MonoBehaviour
    {
        private static readonly int hashHomeRun = Animator.StringToHash("HomeRun");
        private static readonly int hashSpeed = Animator.StringToHash("Speed");

        [Header("Events")]
        [SerializeField] private EventSO homeRunEventSo;

        [Header("Components")]
        [SerializeField] private Animator animator;

        [SerializeField] private ThirdPersonController thirdPersonController;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void HomeRun(EventSO ev)
        {
            animator.SetTrigger(hashHomeRun);
            animator.SetFloat(hashSpeed, 0f);
            thirdPersonController.enabled = false;
        }

        private void RegisterListeners()
        {
            homeRunEventSo.RegisterListener(HomeRun);
        }

        private void UnregisterListeners()
        {
            homeRunEventSo.UnregisterListener(HomeRun);
        }

        private void ValidateReferences()
        {
            animator = GetComponent<Animator>();
            thirdPersonController = GetComponent<ThirdPersonController>();

            Debug.Assert(animator != null, "animator != null");
            Debug.Assert(thirdPersonController != null, "thirdPersonController != null");
            Debug.Assert(homeRunEventSo != null, "homeRunEventSo != null");
        }

    }
}