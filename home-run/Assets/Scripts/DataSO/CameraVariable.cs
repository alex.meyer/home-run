using UnityEngine;

namespace HomeRun
{
    [CreateAssetMenu(fileName = "NewCameraVariable", menuName = "DataSO/CameraVariable", order = 0)]
    public class CameraVariable : BaseVariable<Camera> { }
}