using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace HomeRun
{
    public class OpponentSpawner : MonoBehaviour
    {
        [Header("variables")]
        [SerializeField] private Vector3Variable playerPositionVariable;

        [Header("Config")]
        [SerializeField] private CharacterConfig[] characterConfigs;

        [Header("Components")]
        [SerializeField] private GameObject[] characterPrefabs;

        [Header("Meta Data")]
        [SerializeField] private Vector2 fieldSize = new Vector2(80f, 40f);
        [SerializeField] private float minDistanceToSpawn = 10f;

        private bool _playerPosValid = false;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
            StartCoroutine(WaitForValidPlayerPositionAndSpawn());
        }

        private IEnumerator WaitForValidPlayerPositionAndSpawn()
        {
            while (!_playerPosValid)
            {
                yield return null;
            }

            foreach (var characterConfig in characterConfigs)
            {
                var index = Random.Range(0, characterPrefabs.Length);
                var prefab = characterPrefabs[index];
                var charController = prefab.GetComponentInChildren<CharacterController>();
                var pos = GetSpawnPosition(charController);
                var go = Instantiate(prefab, pos, Quaternion.identity);
                var predictAndFollowAi = go.GetComponentInChildren<PredictAndFollowAI>();
                predictAndFollowAi.CharacterConfig = characterConfig;
            }
        }

        private Vector3 GetSpawnPosition(CharacterController charController)
        {
            var tries = 20;
            var pos = Vector3.zero;

            Debug.Log($"[SPAWN] player position: {playerPositionVariable.Content}");

            while(tries > 0)
            {
                var halfSize = fieldSize * 0.5f;
                pos.x = Random.Range(-halfSize.x, halfSize.x);
                pos.z = Random.Range(-halfSize.y, halfSize.y);
                pos.y = charController.center.y;
                var distance = (pos - playerPositionVariable.Content).magnitude;
                Debug.Log($"[SPAWN] New pos: {pos}, distance: {distance}");
                if (distance >= minDistanceToSpawn)
                {
                    Debug.Log($"[SPAWN] Distance accepted: {distance}");
                    return pos;
                }

                tries--;
            }

            return pos;
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void PlayerPositionChanged(Vector3 newPos)
        {
            _playerPosValid = true;
        }

        private void RegisterListeners()
        {
            playerPositionVariable.RegisterListener(PlayerPositionChanged);
        }

        private void UnregisterListeners()
        {
            playerPositionVariable.UnregisterListener(PlayerPositionChanged);
        }

        private void ValidateReferences()
        {
            Debug.Assert(playerPositionVariable != null, "playerPositionVariable != null");
            Debug.Assert(characterConfigs.Length > 0, "characterConfigs.Length > 0");
            Debug.Assert(characterPrefabs.Length > 0, "characterPrefabs.Length > 0");
        }
    }
}