using UnityEngine;
using UnityEngine.Serialization;

namespace HomeRun
{
    [RequireComponent(typeof(PredictAndFollowAI), typeof(Animator))]
    public class TackleAI : MonoBehaviour
    {
        private static readonly int hashTackle = Animator.StringToHash("Tackle");

        [FormerlySerializedAs("playerTackledEvent")]
        [Header("Events")]
        [SerializeField] private EventSO playerTackledEventSo;

        [Header("Variables")]
        [SerializeField] private Vector3Variable playerPositionVariable;

        [Header("Components")]
        [SerializeField] private PredictAndFollowAI predictAndFollowAI;
        [SerializeField] private Animator animator;

        [Header("Meta Data")]
        [SerializeField] private float distanceToTackle;

        private float _sqrDistanceToTackle;

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
            _sqrDistanceToTackle = distanceToTackle * distanceToTackle;
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void Update()
        {
            var distance = (transform.position - playerPositionVariable.Content).sqrMagnitude;
            if (!(distance < _sqrDistanceToTackle))
            {
                return;
            }

            predictAndFollowAI.Run = false;
            animator.SetTrigger(hashTackle);
            playerTackledEventSo.Invoke();
            this.enabled = false;
        }

        private void RegisterListeners() { }

        private void UnregisterListeners() { }

        private void ValidateReferences()
        {
            predictAndFollowAI = GetComponent<PredictAndFollowAI>();
            animator = GetComponent<Animator>();

            Debug.Assert(playerTackledEventSo != null, "playerTackledEvent != null");
            Debug.Assert(playerPositionVariable != null, "playerPositionVariable != null");
            Debug.Assert(predictAndFollowAI != null, "predictAndFollowAI != null");
            Debug.Assert(animator != null, "animator != null");
        }

    }
}