using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace HomeRun
{
    [RequireComponent(typeof(CharacterController), typeof(Animator))]
    public class PredictAndFollowAI : MonoBehaviour
    {
        private static readonly int hashAnimIDSpeed = Animator.StringToHash("Speed");
        private static readonly int hashAnimIDMotionSpeed = Animator.StringToHash("MotionSpeed");

        [Header("Config")]
        [SerializeField] private CharacterConfig characterConfig;

        [Header("Variables")]
        [SerializeField] private Vector3Variable playerSpeedVariable;
        [SerializeField] private Vector3Variable playerPositionVariable;
        [SerializeField] private CameraVariable mainCameraVariable;

        [Header("Events")]
        [SerializeField] private EventSO homeRunEvent;
        [SerializeField] private EventSO playerTackledEvent;

        [Header("Components")]
        [SerializeField] private CharacterController characterController;
        [SerializeField] private Animator animator;
        [SerializeField] private AudioClip[] footstepAudioClips;
        [Range(0, 1)]
        [SerializeField] private float footstepAudioVolume = 0.5f;

        [Header("Meta Data")]
        [Tooltip("Acceleration and deceleration")]
        [SerializeField] private float speedChangeRate = 10.0f;
        [Range(0.0f, 0.3f)]
        [SerializeField] private float rotationSmoothTime = 0.12f;
        [SerializeField] private bool run;

        private float _animationBlend;
        private float _targetRotation = 0.0f;
        private float _rotationVelocity;
        private float _verticalVelocity;

        public bool Run
        {
            set => run = value;
        }

        public CharacterConfig CharacterConfig
        {
            get => characterConfig;
            set => characterConfig = value;
        }

        private void Awake()
        {
            ValidateReferences();
        }

        private void OnEnable()
        {
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void Start()
        {
            run = true;
        }

        private void Update()
        {
            if (!run)
            {
                Move(Vector2.zero);
                return;
            }

            // read new values from the populated variables and predict the target position
            // calculate the needed speed vector by given speed and predicted player position
            var currentPos = characterController.transform.position;
            var currentPlayerPos = playerPositionVariable.Content;
            var predictedPos = currentPlayerPos + playerSpeedVariable.Content * 0.033f; // one frame later
            var move3d = (predictedPos - currentPos).normalized;
            var move = new Vector2(move3d.x, move3d.z);
            Move(move);

            Debug.Log(
                $"[TACKLE] Move: curr own pos: {currentPos}, curr player pos: {currentPlayerPos}, curr player speed: {playerSpeedVariable.Content}, pred. pos: {predictedPos}, move: {move}");
        }

        private void Move(Vector2 move)
        {
            if (mainCameraVariable.Content == null)
            {
                return;
            }

            // set target speed based on move speed, sprint speed and if sprint is pressed
            var targetSpeed = characterConfig.RunSpeed;

            // a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

            // note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
            // if there is no input, set the target speed to 0
            if (move == Vector2.zero) targetSpeed = 0.0f;

            // a reference to the players current horizontal velocity
            var currentHorizontalSpeed = new Vector3(characterController.velocity.x, 0.0f, characterController.velocity.z).magnitude;

            var speedOffset = 0.1f;
            var inputMagnitude = 1f;

            var speed = 0f;
            // accelerate or decelerate to target speed
            if (currentHorizontalSpeed < targetSpeed - speedOffset ||
                currentHorizontalSpeed > targetSpeed + speedOffset)
            {
                // creates curved result rather than a linear one giving a more organic speed change
                // note T in Lerp is clamped, so we don't need to clamp our speed
                speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude,
                    Time.deltaTime * speedChangeRate);

                // round speed to 3 decimal places
                speed = Mathf.Round(speed * 1000f) / 1000f;
            }
            else
            {
                speed = targetSpeed;
            }

            _animationBlend = Mathf.Lerp(_animationBlend, targetSpeed, Time.deltaTime * speedChangeRate);
            if (_animationBlend < 0.01f)
            {
                _animationBlend = 0f;
            }

            // normalise input direction
            Vector3 inputDirection = new Vector3(move.x, 0.0f, move.y).normalized;

            // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
            // if there is a move input rotate player when the player is moving
            if (move != Vector2.zero)
            {
                _targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg;
                var rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity,
                    rotationSmoothTime);

                // rotate to face input direction relative to camera position
                transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
            }


            Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;

            // move the player
            characterController.Move(targetDirection.normalized * (speed * Time.deltaTime) +
                             new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);

            // update animator if using character
            animator.SetFloat(hashAnimIDSpeed, _animationBlend);
            animator.SetFloat(hashAnimIDMotionSpeed, inputMagnitude);
        }

        private void OnFootstep(AnimationEvent animationEvent)
        {
            if (animationEvent.animatorClipInfo.weight > 0.5f)
            {
                if (footstepAudioClips.Length > 0)
                {
                    var index = Random.Range(0, footstepAudioClips.Length);
                    AudioSource.PlayClipAtPoint(footstepAudioClips[index], transform.TransformPoint(characterController.center), footstepAudioVolume);
                }
            }
        }

        private void HomeRun(EventSO ev)
        {
            run = false;
        }

        private IEnumerator DelayedIdle()
        {
            yield return new WaitForSeconds(2f);

            run = false;
        }

        private void PlayerTackled(EventSO ev)
        {
            StartCoroutine(DelayedIdle());
        }

        private void RegisterListeners()
        {
            homeRunEvent.RegisterListener(HomeRun);
            playerTackledEvent.RegisterListener(PlayerTackled);
        }

        private void UnregisterListeners()
        {
            homeRunEvent.UnregisterListener(HomeRun);
            playerTackledEvent.UnregisterListener(PlayerTackled);
        }

        private void ValidateReferences()
        {
            characterController = GetComponent<CharacterController>();
            animator = GetComponent<Animator>();

            Debug.Assert(characterConfig != null, "characterConfig != null");
            Debug.Assert(characterController != null, "characterController != null");
            Debug.Assert(playerSpeedVariable != null, "playerSpeedVariable != null");
            Debug.Assert(playerPositionVariable != null, "playerPositionVariable != null");
            Debug.Assert(mainCameraVariable != null, "mainCameraVariable != null");
            Debug.Assert(homeRunEvent != null, "homeRunEvent != null");
            Debug.Assert(playerTackledEvent != null, "playerTackledEvent != null");
            Debug.Assert(animator != null, "animator != null");
        }

    }
}