using UnityEngine;

namespace HomeRun
{
    [CreateAssetMenu(fileName = "NewCharacterConfig", menuName = "Config/CharacterConfig")]
    public class CharacterConfig : ScriptableObject
    {
        [SerializeField] private float moveSpeed;
        [SerializeField] private float runSpeed;

        public float MoveSpeed => moveSpeed;
        public float RunSpeed => runSpeed;
    }
}